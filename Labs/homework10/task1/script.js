function moveSquare(event) {
    let Square = document.getElementById('Square');
    let container = document.querySelector('.container');

    let containerRect = container.getBoundingClientRect();

    let x = event.clientX - containerRect.left;
    let y = event.clientY - containerRect.top;

    if (x >= 0 && x <= containerRect.width && y >= 0 && y <= containerRect.height) {
        Square.style.left = x + 'px';
        Square.style.top = y + 'px';
        Square.classList.remove('hidden');
    } else {
        Square.classList.add('hidden');
    }
}
moveSquare()