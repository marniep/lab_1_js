document.addEventListener('DOMContentLoaded', function() {
    const ball = document.getElementById('ball');
    let ballLeft = 150; 
    let ballTop = 150; 

    document.addEventListener('keydown', function(event) {
        const key = event.key;

        switch(key) {
            case 'ArrowUp':
                ballTop -= 10;
                break;
            case 'ArrowDown':
                ballTop += 10;
                break;
            case 'ArrowLeft':
                ballLeft -= 10;
                break;
            case 'ArrowRight':
                ballLeft += 10;
                break;
        }

        ball.style.left = ballLeft + 'px';
        ball.style.top = ballTop + 'px';
    });
});
