function weekdays(){
    let weekdays=new Date()
    let textplace= document.getElementById("container")
    let year= weekdays.getFullYear()
    let weekday=weekdays.getDay()
    let day=weekdays.getDate()
    let month=weekdays.getMonth()
    let hoour=weekdays.getHours()
    let minutes=weekdays.getMinutes()
    let second=weekdays.getSeconds()
    let milisecond= weekdays.getMilliseconds()

    let days=["კვირა","ორშაბათი","სამშაბათი","ოთხშაბათი","ხუთშაბათი","პარასკევი","შაბათი"]
    let months=["იანვარი","თებერვალი","მარტი","აპრილი","მაისი","ივნისის","ივლისი","აგვისტო","სექტემბერი","ოქტომბერი","ნოემბრი","დეკემბერი"];

    finaltext="დღეს არის "+year+" წლის "+months[month]+"ის "+day+" "+days[weekday]+" "+hoour+" საათი, "+minutes+" წთ, "+second+" წამი " + milisecond+" მილიწამი"

    textplace.innerHTML=finaltext
}
