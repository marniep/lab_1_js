const greenSquare = document.getElementById('greenSquare');
let isShrunk = false;

document.addEventListener('keydown', function(event) {
  const currentWidth = greenSquare.clientWidth;
  const currentHeight = greenSquare.clientHeight;

  if (event.key === 'ArrowRight') {
    greenSquare.style.width = '100%';
    greenSquare.style.backgroundColor = 'green';
  } else if (event.key === 'ArrowLeft' && !isShrunk) {
    greenSquare.style.width = '50px';
    greenSquare.style.backgroundColor = 'red';
    isShrunk = true;
  } else if (event.key === 'ArrowLeft' && isShrunk) {
    greenSquare.style.width = '50px';
    greenSquare.style.backgroundColor = 'green';
    isShrunk = false;
  } else if (event.key === 'ArrowDown') {
    greenSquare.style.height = '100%';
    greenSquare.style.backgroundColor = 'green';
  } else if (event.key === 'ArrowUp' && !isShrunk) {
    greenSquare.style.height = '50px';
    greenSquare.style.backgroundColor = 'red';
    isShrunk = true;
  } else if (event.key === 'ArrowUp' && isShrunk) {
    greenSquare.style.height = '50px';
    greenSquare.style.backgroundColor = 'green';
    isShrunk = false;
  }
});
