function moveSquare(direction) {
    const square = document.getElementById('square');
    const container = document.getElementById('container');
    const squareRect = square.getBoundingClientRect();
    const containerRect = container.getBoundingClientRect();
  
    let newX = squareRect.left;
    let newY = squareRect.top;
  
    switch (direction) {
      case 'right':
        newX = containerRect.width - squareRect.width;
        break;
      case 'left':
        newX = 0;
        break;
      case 'up':
        newY = 0;
        break;
      case 'down':
        newY = containerRect.height - squareRect.height;
        break;
      case 'up-right':
        newX = containerRect.width - squareRect.width;
        newY = 0;
        break;
      case 'down-right':
        newX = containerRect.width - squareRect.width;
        newY = containerRect.height - squareRect.height;
        break;
      case 'up-left':
        newX = 0;
        newY = 0;
        break;
      case 'down-left':
        newX = 0;
        newY = containerRect.height - squareRect.height;
        break;
      default:
        break;
    }
  
    square.style.left = `${newX}px`;
    square.style.top = `${newY}px`;
  }
  