function generateRandomNumbersAndClickButton() {
    var randomNumbers = [];
    for (var i = 0; i < 30; i++) {
        randomNumbers.push(Math.floor(Math.random() * 31)); 
    }

    document.write("Random Numbers: " + randomNumbers.join(", ") + "<br>");

    var button = document.createElement("button");
    button.innerHTML = "Click Me";

    button.addEventListener("click", function() {
        alert("Button clicked!");
    });

    var div = document.getElementById("myDiv");
    div.appendChild(button);

    button.click();
}
 generateRandomNumbersAndClickButton()