function calculateCompoundInterest() {
    var principal = parseFloat(document.getElementById("principal").value);
    var rate = parseFloat(document.getElementById("rate").value) / 100;
    var time = parseInt(document.getElementById("time").value);
    var compoundFrequency = parseInt(document.getElementById("compoundFrequency").value);

    var amount = principal * Math.pow((1 + (rate / compoundFrequency)), compoundFrequency * time);
    var compoundInterest = amount - principal;

    document.write("<h2>Final Amount with Interest:</h2>");
    document.write("<p>$" + amount.toFixed(2) + "</p>");

    document.write("<h2>Compound Interest:</h2>");
    document.write("<p>$" + compoundInterest.toFixed(2) + "</p>");
}