function generateOddNumbers() {
    const n = parseInt(document.getElementById('n').value);
    const k = parseInt(document.getElementById('k').value);
    const p = parseInt(document.getElementById('p').value);
    let output = '';

    for (let i = 0; i < n; i++) {
        const randomNumber = Math.floor(Math.random() * (p - k + 1)) + k;
        if (randomNumber % 2 !== 0) {
            output += randomNumber + '<br>';
        } else {
            i--; 
        }
    }

    document.getElementById('output').innerHTML = '<h2>Generated Odd Numbers:</h2>' + (output || '<p>No odd numbers in range.</p>');
}
