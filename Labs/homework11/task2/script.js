function validateName() {
    const name = document.getElementById('name').value.trim();
    const nameError = document.getElementById('nameError');
    if (name === '') {
        nameError.textContent = 'Name cannot be empty';
        return false;
    }
    nameError.textContent = '';
    return true;
}

function validateLastName() {
    const lastName = document.getElementById('lastName').value.trim();
    const lastNameError = document.getElementById('lastNameError');
    if (lastName === '') {
        lastNameError.textContent = 'Last Name cannot be empty';
        return false;
    }
    lastNameError.textContent = '';
    return true;
}

function validateIdNumber() {
    const idNumber = document.getElementById('idNumber').value.trim();
    const idNumberError = document.getElementById('idNumberError');
    if (idNumber.length !== 11 || isNaN(idNumber)) {
        idNumberError.textContent = 'ID Number should consist of 11 digits';
        return false;
    }
    idNumberError.textContent = '';
    return true;
}

function validateEmail() {
    const email = document.getElementById('email').value.trim();
    const emailError = document.getElementById('emailError');
    if (!email.includes('@')) {
        emailError.textContent = 'Invalid email format';
        return false;
    }
    emailError.textContent = '';
    return true;
}

function setCurrentDate() {
    const currentDate = new Date();
    const day = currentDate.getDate();
    const month = currentDate.getMonth() + 1;
    const year = currentDate.getFullYear();
    document.getElementById('registrationDate').value = `${day}:${month}:${year}`;
}

function validateForm() {
    let isValid = true;
    isValid = validateName() && isValid;
    isValid = validateLastName() && isValid;
    isValid = validateIdNumber() && isValid;
    isValid = validateEmail() && isValid;

    if (!isValid) {
        alert('Please correct the errors in the form.');
    }
}

setCurrentDate(); 