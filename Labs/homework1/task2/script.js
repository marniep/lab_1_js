function displayTextWithFontSize(text,fontSize){
    const textElement = document.getElementById("myText")
    textElement.textContent = text;
    textElement.style.fontSize = fontSize + "px";
}

displayTextWithFontSize("Text with Custom Font Size", 74);
