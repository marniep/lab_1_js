function countA(str) {
    let count = 0;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === 'a' || str[i] === 'A') {
            count++;
        }
    }
    document.write("Number of 'a's in the string: " + count);
}
countA("MAriami")