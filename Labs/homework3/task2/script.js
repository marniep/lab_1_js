let numberOfCharacters = 18; 
let randomString = generateRandomString(numberOfCharacters);

function generateRandomString(length) {
    let result = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < length; i++) {
        let randomIndex = Math.floor(Math.random() * characters.length);
        let randomCharacter = characters.charAt(randomIndex);
        result += randomCharacter;
    }

    return result;
}
document.write(randomString);
