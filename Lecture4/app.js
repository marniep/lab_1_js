const person = {
    name:"Shota",
    'lastName':"Tsertsvadze",
    "education":{
        "higher": "Yes",
        University:"Georgian American University",
        GPA:4.3
    },
    hobbies:["fishing", "travelling", "reading"],
    getFullName:function(){
        console.log(this.name, this.lastName)
    } 
}

console.log(person)
console.log(person.name)
console.log(person['name'])
console.log(person.education.GPA)
console.log(person.hobbies[1])
person.getFullName()

person.name = "Shotiko"
console.log(person)